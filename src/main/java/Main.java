import recipesmanager.recipes.Food;
import recipesmanager.users.Person;

public class Main {

    public static void main(String[] args){

        Person brad = new Person("Pitt", "Brad");

        System.out.println("Frigo de " + brad.getFirstname() + " " + brad.getLastname());
        System.out.println(brad.getFridge());

        brad.addInMyFridge(new Food("Riz"));
        System.out.println(brad.getFridge());

        brad.addInMyFridge(new Food("Potimarron"));
        System.out.println(brad.getFridge());

        brad.addInMyFridge(new Food("Riz"));
        System.out.println(brad.getFridge());

        brad.removeInMyFridge(new Food("Riz"));
        System.out.println(brad.getFridge());

        brad.removeInMyFridge(new Food("Riz"));
        System.out.println(brad.getFridge());
    }
}
