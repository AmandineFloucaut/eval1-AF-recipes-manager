package recipesmanager.recipes;

public class Food {

    protected final String name;

    public Food(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return getName() ;
    }
}
