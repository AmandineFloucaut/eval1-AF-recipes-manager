package recipesmanager.recipes;

import recipesmanager.recipes.Food;

import java.util.ArrayList;
import java.util.List;

public class Fridge {

    private List<Food> foods;

    public Fridge(){
        this.foods = new ArrayList<Food>();
    }

    /**
     * Method to display food's number in foods list
     * @return foods list size wich represent number of foods
     */
    public int getNbFood(){
        return foods.size();
    }

    /**
     * Method for check if food exist in list, browse all element for compare name food
     * @param food : new instance of Food class
     * @return true if food exist else false
     */
    public boolean existsIn(Food food){
        for(Food f: this.foods){
            if(f.getName().equals(food.getName())){
                return true;
            }
        }
        return false;
    }

    public void add(Food food){
        if(!this.existsIn(food)){
            foods.add(food);
        }
    }

    /**
     * Method for remove one food on foods list.
     * Browse each element for retrieve index of current food (in argument method) and remove this with it
     * @param food new instance of Food class
     */
    public void remove(Food food){
        if(this.existsIn(food)){
            int key = 0;
            for(Food f: this.foods){
                if(f.getName().equals(food.getName())){
                    key = foods.indexOf(f);
                }
            }
            foods.remove(key);
            //foods.remove(foods.indexOf(food));
        }
    }

    @Override
    public String toString() {
        String foodList = "";
        for(Food food: this.foods){
            foodList += food.getName() + ", ";
        };
        return getNbFood() + " aliment(s) dans le frigo : " + foodList;
    }
}
