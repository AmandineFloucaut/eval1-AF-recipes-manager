package recipesmanager.recipes;

public class Ingredient extends Food {

    private int quantity;
    private MeasureUnit measureUnit;

    public Ingredient(String name, int quantity, String measureUnit){
        super(name);
        this.quantity = quantity;

        switch(measureUnit.toUpperCase()){
            case "UNITE" -> this.measureUnit = MeasureUnit.UNITE;
            case "GRAMME" -> this.measureUnit = MeasureUnit.GRAMME;
            case "VERRE" -> this.measureUnit = MeasureUnit.VERRE;
        }
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return getName() ;
    }
}
