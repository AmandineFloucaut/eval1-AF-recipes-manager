package recipesmanager.recipes;

public enum MeasureUnit {
    UNITE,
    GRAMME,
    VERRE
}
