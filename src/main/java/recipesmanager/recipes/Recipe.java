package recipesmanager.recipes;

import recipesmanager.users.Cook;

public class Recipe {

    private String name;
    private Cook author;
    private Ingredient[] ingredients;

    public Recipe(String name, Cook author, Ingredient[] ingredients){
        if(ingredients.length == 0){
            System.out.println("Vous ne pouvez pas créer une nouvelle recette sans ingrédients !");
        }
        else {
            this.name = name;
            this.author = author;
            this.ingredients = ingredients;
        }
    }
}
