package recipesmanager.users;

public class Cook extends Person {

    protected String restaurantName;

    public Cook(String lastname, String firstname, String restaurantName){
        super(lastname, firstname);
        this.restaurantName = restaurantName;
        this.author = true;
    };

    public String getRestaurantName() {
        return restaurantName;
    }

}
