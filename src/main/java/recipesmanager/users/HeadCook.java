package recipesmanager.users;

public class HeadCook extends Cook {

    private int nbStars;

    public HeadCook(String lastname, String firstname, String restaurantName, int nbStars){
        super(lastname, firstname, restaurantName);
        this.nbStars = nbStars;
    };

    public int getNbStars() {
        return nbStars;
    }
}
