package recipesmanager.users;

import recipesmanager.recipes.Food;
import recipesmanager.recipes.Fridge;

public class Person {

    protected String lastname;
    protected String firstname;
    protected Fridge fridge;
    protected Boolean author = false;

    public Person(String lastname, String firstname){
        this.lastname = lastname;
        this.firstname = firstname;
        this.fridge = new Fridge();
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public Fridge getFridge() {
        return fridge;
    }

    public void addInMyFridge(Food food){
        if(!fridge.existsIn(food)){
            this.fridge.add(food);
        }
    }

    public void removeInMyFridge(Food food){
        this.fridge.remove(food);
    }
}
