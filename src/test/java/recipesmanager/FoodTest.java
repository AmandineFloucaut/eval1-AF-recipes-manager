package recipesmanager;
import org.junit.jupiter.api.*;
import recipesmanager.recipes.Food;

import static org.junit.jupiter.api.Assertions.*;

public class FoodTest {

    Food food = new Food("tomate");

    @BeforeAll
    static void initAll() {
        System.out.println("Lancement des tests Account");
    }

    @BeforeEach
    void init() {
        System.out.println("Lancement d'un test Account");
    }

    @Test
    void testReturnName(){

        assertEquals("tomate", food.getName());
    }


    @AfterEach
    void tearDown() {
        System.out.println("Fin du test Account");
    }

    @AfterAll
    static void tearDownAll() {
        System.out.println("Fin des tests Account");
    }
}
