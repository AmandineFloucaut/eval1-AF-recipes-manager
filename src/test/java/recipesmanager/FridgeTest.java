package recipesmanager;

import org.junit.jupiter.api.*;
import recipesmanager.recipes.Food;
import recipesmanager.recipes.Fridge;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FridgeTest {

    Fridge fridge = new Fridge();

    @BeforeAll
    static void initAll() {
        System.out.println("Lancement des tests Account");
    }

    @BeforeEach
    void init() {
        System.out.println("Lancement d'un test Account");
    }

    @Test
    void testNbFoodListEmpty(){
        assertEquals(0, fridge.getNbFood());
    }

    @Test
    void testExistIn(){
        assertEquals(false, fridge.existsIn(new Food("Tomate")));
    }

    @Test
    void testAddMethodWithFoodExisting(){
        Food newFood = new Food("Tomate");
        fridge.add(newFood);
        fridge.add(newFood);
        assertEquals(1, fridge.getNbFood());
    }

    @Test
    void testRemoveMethodWithFoodNotExisting(){
        Food newFood = new Food("Tomate");
        Food newFood2 = new Food("Pain");
        fridge.add(newFood);
        fridge.remove(newFood2);
        assertEquals(1, fridge.getNbFood());
    }

    @Test
    void testRemoveMethodWithFoodExisting(){
        Food newFood = new Food("Tomate");
        Food newFood2 = new Food("Pain");
        fridge.add(newFood);
        fridge.add(newFood2);
        fridge.remove(newFood2);
        assertEquals(1, fridge.getNbFood());
    }

    @AfterEach
    void tearDown() {
        System.out.println("Fin du test Account");
    }

    @AfterAll
    static void tearDownAll() {
        System.out.println("Fin des tests Account");
    }
}
