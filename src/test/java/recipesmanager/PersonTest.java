package recipesmanager;

import org.junit.jupiter.api.*;
import recipesmanager.users.Person;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PersonTest {

    Person person = new Person("Joli", "Angelina");

    @BeforeAll
    static void initAll() {
        System.out.println("Lancement des tests Account");
    }

    @BeforeEach
    void init() {
        System.out.println("Lancement d'un test Account");
    }

    @Test
    void testReturnFullName(){

        assertEquals("Angelina Joli", (person.getFirstname() + " " + person.getLastname()));
    }


    @AfterEach
    void tearDown() {
        System.out.println("Fin du test Account");
    }

    @AfterAll
    static void tearDownAll() {
        System.out.println("Fin des tests Account");
    }
}
